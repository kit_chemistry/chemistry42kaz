var audio = [];
	
//SOUNDS
var buttonSound, fireworkSound, speechSound;

//OTHER GLOBAL DATA
var shots, currentShot,
	globalName,
	sprite = [],
	timeout = [],
	launch = [];

var setMenuStuff = function(){
	var enterButton = $(".enter-button"),
		annotation = $("#frame-000 .annotation-hidden"),
		annotationButton = $("#frame-000 .annotation-button"),
		password = $(".password"),
		error = $(".error"),
		name = $(".name");
		name.val("");
		password.val("");
		
		
		sayHello = function(){
		regBox.html("Здравствуйте, " + name.val() + "!");
		regBox.css("height", "3em");
		regBox.css("padding", "0.5em");
		regBox.css("text-align", "center");
		globalName = name.val();
		regBox.addClass("topcorner");
		timeout[0] = setTimeout(function(){
			regBox.html(name.val());
			regBox.css("width", name.val().length + "em");
		}, 3000);
	};
	
	if(globalName)
		sayHello();
	
	var annotationButtonListener = function(){
		annotation.toggleClass("annotation-shown");
		annotationButton.toggleClass("annotation-button-close");

	};
	annotationButton.off("click", annotationButtonListener);
	annotationButton.on("click", annotationButtonListener);
	
	var enterButtonListener = function(){
		if(password.val() === "12345")
			sayHello();
		else
			error.html("неверный пароль");
	};
	enterButton.off("click", enterButtonListener);
	enterButton.on("click", enterButtonListener);
}


var setLRSData = function(){
	tincan = new TinCan (
    {
        recordStores: [
            {
                "endpoint": "http://54.154.57.220/data/xAPI/",
				"username": "e083498f4e256e68ab2c5ae2be4195d9a348eb20",
				"password": "c6ea3c32a9d77919d0eb3cdea3bc5d460f50d93b"
            }
        ]
    });
}

var sendLaunchedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/launched"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var sendCompletedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz/chemistry"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/completed"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

Motio.prototype.addListeners = function(currFPS, once, onFrame){
	var self = this;
	
	if(once)
		self.on("frame", onFrame);
		
	window.addEventListener("resize", function(){
		var currActiveFrame = self.frame,
			numFrames = self.frames,
			currElement = self.element,
			newWidth = self.element.style.width,
			statePaused = self.isPaused;
		
		self.destroy()
		
		self = new Motio(currElement, {
			fps: currFPS,
			frames: numFrames,
			bgWidth: newWidth
		});
		
		if(once)
		{
			self.on("frame", function(){
				if(this.frame === this.frames - 1)
				{
					this.pause();	
				}
			});
		}
		
		self.to(currActiveFrame, true);
		
		if(!statePaused)
		{
			timeout[3] = setTimeout(function(){
				if(self.frame !== self.frames - 1 || !once)
					self.play();
			}, 50);
		}
	});
};

var plusOneSec = function(audiofile)
{
	var currTime = Math.round(audiofile.currentTime);
	audiofile.currentTime = ++currTime;
}

launch["frame-000"] = function()
{
	theFrame = $("#frame-0"),
	theClone = theFrame.clone();
	fadeNavsOut();
}

launch["frame-101"] = function()
{
		theFrame = $("#frame-101"),
		theClone = theFrame.clone();
	var	firework = $("#frame-101"),
		boys = $("#boys-talking"), 
		boysEyes = $(".boys-eyes-animated"), 
		leftBoyMouth = $("#left-boy-mouth"),
		rightBoyMouth = $("#right-boy-mouth");
		
	audio[0] = new Audio("audio/firework.mp3");
	audio[1] = new Audio("audio/s1-1.mp3");
	
	sprite[0] = new Motio(firework[0], {
		fps: 5,
		frames: 7
	});
	
	sprite[0].addListeners(5, false);
	
	rightBoyMouth.fadeOut(0);
	leftBoyMouth.fadeOut(0);
	boys.fadeOut(0);
	boysEyes.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[1].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 2000);
	});
	
	audio[0].addEventListener("ended", function(){
		boys.fadeIn(500);
		boysEyes.fadeIn(0);
		timeout[2] = setTimeout(function(){
			audio[1].play();
			leftBoyMouth.fadeIn(0);			
		}, 1000);
		timeout[3] = setTimeout(function(){
			leftBoyMouth.fadeOut(0);
			timeout[0] = setTimeout(function(){
				rightBoyMouth.fadeIn(0);
			}, 500)
		}, 3000);
		timeout[4] = setTimeout(function(){
			rightBoyMouth.fadeOut(0);
		}, 12000);	
	});
	
	startButtonListener = function(){
		audio[0].play();
		sprite[0].play();
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(1);
	}, 2000);
}

launch["frame-102"] = function()
{
	theFrame = $("#frame-102");
	theClone = theFrame.clone();
	
	theFrame.css("opacity", "1");
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAutoNGS();
			sendCompletedStatement(2);
		}, 5000);	
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(2);
	}, 2000);
}

launch["frame-102-a"] = function()
{
	theFrame = $("#frame-102-a");
	theClone = theFrame.clone();
	
	fadeNavsOut();
	fadeLauncherIn();
	audio[0] = new Audio("audio/s2-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(2);
	});
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audio[0].play();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(2);
	}, 2000);
}

launch["frame-103"] = function()
{
	theFrame = $("#frame-103");
	theClone = theFrame.clone();
	var elem = $("#frame-103 .elem"),
		ar = $("#frame-103 .ar"),
		co2 = $("#frame-103 .co2"),
		h2o = $("#frame-103 .h2o"),
		o2 = $("#frame-103 .o2"),
		n2 = $("#frame-103 .n2");

	fadeNavsOut();
	fadeLauncherIn();
	elem.fadeOut(0);
	
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			n2.fadeIn(500);
		}, 4000);
		timeout[2] = setTimeout(function(){
			n2.fadeOut(500);
			o2.fadeIn(500);
		}, 5000);
		timeout[3] = setTimeout(function(){
			o2.fadeOut(500);
			co2.fadeIn(500);
		}, 7000);
		timeout[4] = setTimeout(function(){
			co2.fadeOut(500);
			ar.fadeIn(500);
		}, 8000);
		timeout[5] = setTimeout(function(){
			ar.fadeOut(500);
			h2o.fadeIn(500);
		}, 10000);
		timeout[6] = setTimeout(function(){
			h2o.fadeOut(500);
		}, 12000);
		timeout[6] = setTimeout(function(){
			audio[0].pause();
		}, 15000);
		timeout[7] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			hideEverythingBut($("#frame-103-a"));
			sendCompletedStatement(3);
		}, 16000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		theFrame.attr("data-done", "true");
		sendLaunchedStatement(3);
	}, 2000);
}

launch["frame-103-a"] = function()
{
	theFrame = $("#frame-103-a");
	theClone = theFrame.clone();	
	
	audio[0] = new Audio("audio/s3-1.mp3");
	
	var reactionAnimated = $("#frame-103-a #reaction-animated");
	
	sprite[0] = new Motio(reactionAnimated[0], {
		fps: 3,
		frames: 25
	});
	
	sprite[0].addListeners(3, true, function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();	
		}
	})
	
	audio[0].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(3);
		}, 2000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			sprite[0].play();
			audio[0].play();
		}, 2000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(3);
	}, 2000);
}

launch["frame-104"] = function()
{
		theFrame = $("#frame-104"),
		theClone = theFrame.clone();
	var injectorAnimated = document.querySelector("#injector-animated"), 
		burner = $("#burner"),
		burnerStart = $("#burner-start"),
		trigger = $("#burner-trigger"),
		fields = $("#frame-104 .field"), 
		airField = $("#air-field"),
		gasField = $("#gas-field"),
		oxygenField = $("#oxygen-field"),
		checkButton = $("#frame-104 #check-button");
		
	burner.fadeOut(0);
	
	if(airField.val().length && gasField.val().length
			&& oxygenField.val().length)
		checkButton.fadeIn(0);
	else
		checkButton.fadeOut(0);
	
	sprite[0] = new Motio(injectorAnimated, {
		fps: 1,
		frames: 7
	});
	
	sprite[1] = new Motio(burnerStart[0], {
		fps: 2,
		frames: 3
	});
	
	sprite[0].addListeners(1, true, function(){
		if(this.frame === this.frames - 1)
		{		
			this.pause();
			burner.fadeOut(0);
		}
	});
	
	burnerStart.fadeOut(0);
	
	sprite[1].addListeners(2, true, function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			sprite[0].play();
			burner.fadeIn(0);
			burnerStart.fadeOut(0);
		}
	});
		
	startButtonListener = function()
	{
		burnerStart.fadeIn(0);
		sprite[1].play();
	}
	
	var keyupListener = function()
	{
		if(airField.val().length && gasField.val().length
			&& oxygenField.val().length)
			{
				checkButton.fadeIn(0);	
				checkButton.addClass("transition-1s");
				checkButton.css("backgroundColor", "#B1C72D");
				timeout[0] = setTimeout(function(){
					checkButton.css("backgroundColor", "");
					checkButton.removeClass("transition-1s");
				}, 1000);
			}
		else
			checkButton.fadeOut(0);	
	}
	
	fields.val("");
	fields.off("keyup", keyupListener);
	fields.on("keyup", keyupListener);
	fadeNavsOut();
	fadeLauncherIn();
	
	var clickListener = function(){
		for(var i = 0; i < fields.length; i++)
		{
			currField = $(fields[i]);
			
			if(currField.attr("data-correct") === currField.val())
				currField.css("color", "green");
			else
				currField.css("color", "red");
			
			currField.css("font-weight", "bold");
		}
		
		timeout[0] = setTimeout(function(){
			for(var i = 0; i < fields.length; i++)
		{
			currField = $(fields[i]);
			
			currField.css("color", "green");
			currField.val(currField.attr("data-correct"));
		}
		}, 3000);
		
		timeout[1] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(4);
		}, 6000);
	};
	checkButton.off("click", clickListener);
	checkButton.on("click", clickListener);

	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(4);
	}, 2000);
}

launch["frame-105"] = function()
{
		theFrame = $("#frame-105"),
		theClone = theFrame.clone();
	var candleBurning = $("#candle-burning-animated"),
		candleGoOut = $("#candle-go-out-animated"),
		glassAndBox = $("#glass-and-box"),
		lidClose = $("#lid-close-animated"),
		cobaltChange = $("#cobalt-change-animated");
	
	audio[0] = new Audio("audio/s5-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			sprite[0].play();
		}, 3000);
	});
	
	sprite[1] = new Motio(cobaltChange[0], {
		fps: 3, 
		frames: 8
	});
	sprite[0] = new Motio(lidClose[0], {
		fps: 2,
		frames: 12
	});
	sprite[2] = new Motio(candleGoOut[0], {
		fps: 3, 
		frames: 4
	});
	
	sprite[2].addListeners(3, true, function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			
			timeout[1] = setTimeout(function(){
				sprite[0].play();
			}, 1000);
		}
	});
	
	sprite[1].addListeners(3, true, function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			timeout[0] = setTimeout(function(){
				theFrame.attr("data-done", "true");
				sendCompletedStatement(5);
				fadeNavsInAuto();
			}, 2000);
		}
	});
	
	sprite[0].addListeners(2, true, function(){
		if(this.frame === 5)
		{
			this.pause();
			timeout[2] = setTimeout(function(){
				candleBurning.fadeOut(0);
				candleGoOut.fadeIn(0);
				sprite[2].play();
			}, 2000);
		}
		
		if(this.frame === this.frames - 1)
		{
			this.pause();
			sprite[1].play();
		}
	});
	
	candleGoOut.fadeOut(0);
	candleBurning.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function()
	{
		timeout[0] = setTimeout(function(){
			audio[0].play();
		}, 1000);
		candleBurning.fadeIn(0);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(5);
	}, 2000);
}

launch["frame-106"] = function()
{
		theFrame = $("#frame-106"),
		theClone = theFrame.clone();
	var draggables = $("#frame-106 .ball"),
		draggabillies = [],
		initialX, initialY,
		vegetable, basket;
	
	audio[0] = new Audio("audio/s7-1.mp3");
	
	for(var i = 0; i < draggables.length; i++)
	{
		draggabillies[i] = new Draggabilly(draggables[i]);
	}
	
	var onStart = function(instance, event, pointer){
		vegetable = $(event.target);
		vegetable.addClass("isDragging");
	};
	
	var onEnd = function(instance, event, pointer){
		vegetable.removeClass("isDragging");
		vegetable.fadeOut(0);
		
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));		
		
		if(vegetable.attr("data-key") === basket.attr("data-key"))
		{
			basket.html(vegetable.html());
			basket.css("background-color", "#1597CE");
			basket.css("color", "white");
			vegetable.remove();
			
			if(!$("#frame-106 .ball").length)
			{
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(6);
			}
		}
		else
		{
			vegetable.fadeIn(0);
			vegetable.css("left", "");
			vegetable.css("top", "");
		}
	};
	
	for(var i = 0; i < draggables.length; i ++)
	{
		draggabillies[i].on("dragStart", onStart);
		draggabillies[i].on("dragEnd", onEnd);
	}
	
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audio[0].play();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(6);
	}, 2000);
}

launch["frame-107"] = function()
{
		theFrame = $("#frame-107"),
		theClone = theFrame.clone();
	var draggables = $("#frame-107 .ball");
	var draggabillies = [];
	var initialX, initialY;
	var vegetable, basket;
	
	
	
	for(var i = 0; i < draggables.length; i++)
	{
		draggabillies[i] = new Draggabilly(draggables[i]);
	}
	
	var onStart = function(instance, event, pointer){
		vegetable = $(event.target);
		vegetable.addClass("isDragging");
	};
	
	var onEnd = function(instance, event, pointer){
		$(event.target).removeClass("isDragging");
		
		vegetable.fadeOut(0);
		
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));		
		
		if(vegetable.attr("data-key") === basket.attr("data-key"))
		{
			basket.html(vegetable.html());
			vegetable.remove();
			
			if(!$("#frame-107 .ball").length)
			{
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(7);
			}
		}
		else
		{
			basket.css("background-color", "");
			basket.css("color", "");
			vegetable.fadeIn(0);
			vegetable.css("left", "");
			vegetable.css("top", "");
		}
	};
	
	for(var i = 0; i < draggables.length; i ++)
	{
		draggabillies[i].on("dragStart", onStart);
		draggabillies[i].on("dragEnd", onEnd);
	}
	
	
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(7);
	}, 2000);
}

launch["frame-108"] = function()
{
		theFrame = $("#frame-108"),
		theClone = theFrame.clone();
	var	firework = $("#frame-108");
	
	audio[0] = new Audio("audio/firework.mp3");
	
	sprite[0] = new Motio(firework[0], {
		fps: 5,
		frames: 7
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		sprite[0].play();
		audio[0].play();
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(8);
		}, 5000);	
	};
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(8);
	}, 2000);
}

launch["frame-201"] = function()
{
	theFrame = $("#frame-201"),
	theClone = theFrame.clone();
	
	sendLaunchedStatement(21);
}

launch["frame-202"] = function()
{
	theFrame = $("#frame-202"),
	theClone = theFrame.clone();
	
	sendLaunchedStatement(22);
}

launch["frame-203"] = function()
{
	theFrame = $("#frame-203"),
	theClone = theFrame.clone();
	
	sendLaunchedStatement(23);
}

launch["frame-204"] = function()
{
	theFrame = $("#frame-204"),
	theClone = theFrame.clone();
	
	sendLaunchedStatement(24);
}

launch["frame-205"] = function()
{
	theFrame = $("#frame-205"),
	theClone = theFrame.clone();
	
	sendLaunchedStatement(25);
}

launch["frame-206"] = function()
{
	theFrame = $("#frame-206"),
	theClone = theFrame.clone();
	
	sendLaunchedStatement(25);
}

launch["frame-301"] = function()
{
	theFrame = $("#frame-301"),
	theClone = theFrame.clone();
	var researchVideoButton = $(".research-video-button");

	fadeNavsOut();
	sendLaunchedStatement(31);
	initMenuButtons();
	
	researchVideoButton.click(function(){
		hideEverythingBut($("#frame-302"));
	});
}

launch["frame-302"] = function()
{
	theFrame = $("#frame-302"),
	theClone = theFrame.clone();
	var videoContainer = $("#frame-302 .video-container"),
		video = $("#frame-302 .video");
	
	fadeNavsOut();
	fadeLauncherIn();
	
	video.attr("width", videoContainer.css("width"));
	video.attr("height", videoContainer.css("height"));
	
	$(window).resize(function(){
		video.attr("width", videoContainer.css("width"));
		video.attr("height", videoContainer.css("height"));
	});
	
	video[0].addEventListener("ended", function(){
		fadeNavsIn();
	});
	
	startButtonListener = function(){
		video[0].play();
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(32);
	}, 2000);
}

var hideEverythingBut = function(elem)
{
	frames = $(".frame");
	
	if(theFrame)
		theFrame.html(theClone.html());
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	regBox = $(".reg-box");
	fadeTimerOut();
		
	if(!globalName)
		regBox.fadeOut(0);
	
	if(elem.attr("id") === "frame-000" || elem.hasClass("fact"))
	{
		fadeNavsOut();
		fadeTimerOut();
	}
	
	for(var i = 0; i < audio.length; i ++)
		audio[i].pause();
	
	for(var i = 0; i < sprite.length; i ++)
		sprite[i].pause();
	
	for(var i = 0; i < timeout.length; i++)
		clearTimeout(timeout[i]);
	
	if(elem.attr("id") === "frame-000")
		regBox.fadeIn(0);
	
	launch[elem.attr("id")]();
	
	initMenuButtons();
}

var initMenuButtons = function(){
	
	$(".link").on("click", function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var showModel = function(content){
	model.fadeIn(1000);
	modelContent.html(content);
};

var hideModel = function(){
	model.fadeOut(1000);
};

var showMap = function()
{
	
}
var main = function()
{
	var video = $(".intro-video"),
		pic = $(".intro-pic");
	
	hideEverythingBut($("#frame-000"));
	
	setMenuStuff();
	video.attr("width", video.parent().css("width"));
	video.attr("height", video.parent().css("height"));
	video[0].play();
	
	timeout[0] = setTimeout(function(){
		video.hide();
	}, 10000);
	timeout[1] = setTimeout(function(){
		pic.hide(); 
	}, 15000);
};

$(document).ready(main);